# Toaster

### Install

`yarn install` or `npm install`

### Start

`yarn start` or `npm start`

![example](https://pbs.twimg.com/media/D2DOp5YWkAE9xCn.png)

### Test

`yarn test` or `npm test`

### Lint

`yarn lint` or `npm run lint`
