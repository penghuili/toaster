import { MessageType } from '../models/message';
import { getClassName } from './class-name';

describe('class-name:', () => {
  it('should get alert className', () => {
    const type = MessageType.Alert;
    const className = getClassName({
      'toaster-item': true,
      'toaster-item--alert': type === MessageType.Alert,
    });
    expect(className).toBe('toaster-item toaster-item--alert');
  });
});
