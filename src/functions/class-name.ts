/**
 * Helper function to get the className for an element, based on some conditions
 * @param conditions An object, keys are classNames, values are boolean values
 *
 * For example:
 * ```
 * const type = MessageType.Info;
 * const className = getClassName({
 *  'toaster-item': true,
 *  'toaster-item--alert': type === MessageType.Alert,
 * })
 * ```
 */
export function getClassName(conditions: {[className: string]: boolean}): string {
  return Object.keys(conditions)
    .filter(k => conditions[k])
    .join(' ');
}
