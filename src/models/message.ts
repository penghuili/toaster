export interface Message {
  id: number;
  text: string;
  position: MessagePosition;
  type: MessageType;
}
export enum MessagePosition {
  TopLeft = 'TopLeft',
  TopRight = 'TopRight',
  BottomLeft = 'BottomLeft',
  BottomRight = 'BottomRight',
}
export enum MessageType {
  Alert = 'Alert',
  Info = 'Info',
  Warning = 'Warning',
}
