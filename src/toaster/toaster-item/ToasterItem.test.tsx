/* tslint:disable:no-unsafe-any */
import React from 'react';
import ReactDOM from 'react-dom';

import { Message, MessagePosition, MessageType } from '../../models/message';
import ToasterItem from './ToasterItem';

describe('ToasterItem', () => {
  it('should render nothing if there is no message', () => {
    const div: HTMLElement = document.createElement('div');
    ReactDOM.render(<ToasterItem message={null} />, div);

    const textContent = div.textContent || '';
    expect(textContent.trim()).toBe('');
  });

  it('should show an alert', () => {
    const message: Message = {
      id: 123,
      text: 'test 1',
      position: MessagePosition.TopLeft,
      type: MessageType.Alert,
    };
    const div: HTMLDivElement = document.createElement('div');
    ReactDOM.render(<ToasterItem message={message} />, div);

    const textContent = (div.querySelector('.mst-toaster-item--alert') as HTMLDivElement).textContent || '';
    expect(textContent.trim()).toBe(message.text);
  });

  it('should show an info', () => {
    const message: Message = {
      id: 123,
      text: 'test 1',
      position: MessagePosition.TopLeft,
      type: MessageType.Info,
    };
    const div: HTMLDivElement = document.createElement('div');
    ReactDOM.render(<ToasterItem message={message} />, div);

    const textContent = (div.querySelector('.mst-toaster-item--info') as HTMLDivElement).textContent || '';
    expect(textContent.trim()).toBe(message.text);
  });
  it('should show a warning', () => {
    const message: Message = {
      id: 123,
      text: 'test 1',
      position: MessagePosition.TopLeft,
      type: MessageType.Warning,
    };
    const div: HTMLDivElement = document.createElement('div');
    ReactDOM.render(<ToasterItem message={message} />, div);

    const textContent = (div.querySelector('.mst-toaster-item--warning') as HTMLDivElement).textContent || '';
    expect(textContent.trim()).toBe(message.text);
  });
});
