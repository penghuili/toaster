import './ToasterItem.scss';

import React, { PureComponent } from 'react';

import { getClassName } from '../../functions/class-name';
import { Message, MessageType } from '../../models/message';

interface Props {
  key?: number;
  message: Message | null;
}
interface State {
  message: Message | null;
}

/**
 * `ToasterItem` show a toaster message based on the message type
 */
class ToasterItem extends PureComponent<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      message: props.message,
    };
  }

  render() {
    if (!this.state.message) {
      return null;
    }

    return (
      <div
        className={getClassName({
          'mst-toaster-item': true,
          'mst-toaster-item--alert': this.state.message.type === MessageType.Alert,
          'mst-toaster-item--info': this.state.message.type === MessageType.Info,
          'mst-toaster-item--warning': this.state.message.type === MessageType.Warning,
        })}
      >
        {this.state.message.text}
      </div>
    );
  }
}

export default ToasterItem;
