import React from 'react';
import ReactDOM from 'react-dom';

import { MessagePosition, MessageType } from '../models/message';
import { ToasterService } from '../services/toaster.service';
import Toaster from './Toaster';

it('should show no message', () => {
  ToasterService.messages$.next([]);

  const div = document.createElement('div');
  ReactDOM.render(<Toaster />, div);

  const textContent = div.textContent as string;
  expect(textContent.trim()).toBe('');
});
it('should show messages correctly', () => {
  const topLeftMessage1 = {
    id: 1,
    text: 'text 1',
    position: MessagePosition.TopLeft,
    type: MessageType.Alert,
  };
  const topLeftMessage2 = {
    id: 2,
    text: 'text 2',
    position: MessagePosition.TopLeft,
    type: MessageType.Alert,
  };
  const topRightMessage = {
    id: 3,
    text: 'text 3',
    position: MessagePosition.TopRight,
    type: MessageType.Alert,
  };
  const bottomLeftMessage = {
    id: 4,
    text: 'text 4',
    position: MessagePosition.BottomLeft,
    type: MessageType.Alert,
  };
  const bottomRightMessage1 = {
    id: 5,
    text: 'text 5',
    position: MessagePosition.BottomRight,
    type: MessageType.Alert,
  };
  const bottomRightMessage2 = {
    id: 6,
    text: 'text 6',
    position: MessagePosition.BottomRight,
    type: MessageType.Alert,
  };
  ToasterService.messages$.next([
    topLeftMessage1,
    topLeftMessage2,
    topRightMessage,
    bottomLeftMessage,
    bottomRightMessage1,
    bottomRightMessage2,
  ]);
  const div = document.createElement('div');
  ReactDOM.render(<Toaster />, div);

  const topLeftBlock = div.querySelector('.mst-toaster--tl') as HTMLElement;
  const topLefElements = Array.from(topLeftBlock.querySelectorAll('.mst-toaster-item')) as HTMLElement[];
  expect(topLefElements.length).toBe(2);
  expect(topLefElements[0].textContent).toContain(topLeftMessage1.text);
  expect(topLefElements[1].textContent).toContain(topLeftMessage2.text);

  const topRightBlock = div.querySelector('.mst-toaster--tr') as HTMLElement;
  const topRightElements = Array.from(topRightBlock.querySelectorAll('.mst-toaster-item')) as HTMLElement[];
  expect(topRightElements.length).toBe(1);
  expect(topRightElements[0].textContent).toContain(topRightMessage.text);

  const bottomLeftBlock = div.querySelector('.mst-toaster--bl') as HTMLElement;
  const bottomLeftElements = Array.from(bottomLeftBlock.querySelectorAll('.mst-toaster-item')) as HTMLElement[];
  expect(bottomLeftElements.length).toBe(1);
  expect(bottomLeftElements[0].textContent).toContain(bottomLeftMessage.text);

  const bottomRightBlock = div.querySelector('.mst-toaster--br') as HTMLElement;
  const bottomRightElements = Array.from(bottomRightBlock.querySelectorAll('.mst-toaster-item')) as HTMLElement[];
  expect(bottomRightElements.length).toBe(2);
  expect(bottomRightElements[0].textContent).toContain(bottomRightMessage1.text);
  expect(bottomRightElements[1].textContent).toContain(bottomRightMessage2.text);
});
