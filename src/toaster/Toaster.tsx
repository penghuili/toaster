import './Toaster.scss';

import React, { Component } from 'react';
import { Subscription } from 'rxjs';

import { getClassName } from '../functions/class-name';
import { Message, MessagePosition } from '../models/message';
import { ToasterService } from '../services/toaster.service';
import ToasterItem from './toaster-item/ToasterItem';

interface State {
  messages: Message[];
}

/**
 * `Toaster` component subscribes to the message changes in `ToasterService`,
 * and show messages at the right place: top left, top right, bottom left, bottom right.
 *
 * Use `Toaster` component in a top level component(e.g. the root component)
 */
class Toaster extends Component<{}, State> {
  private sub: Subscription | null = null;

  constructor(props: {}) {
    super(props);
    this.state = {
      messages: [],
    };
  }

  componentDidMount() {
    this.sub = ToasterService.getMessages().subscribe((messages) => {
      this.setState({ messages });
    });
  }
  componentWillUnmount() {
    if (this.sub) {
      this.sub.unsubscribe();
    }
  }

  getMessageElements = (messages: Message[], position: MessagePosition) => {
    const filtered = messages.filter(m => m.position === position);
    return filtered.length > 0
      ? (
        <div
          className={getClassName({
            'mst-toaster': true,
            'mst-toaster--tl': position === MessagePosition.TopLeft,
            'mst-toaster--tr': position === MessagePosition.TopRight,
            'mst-toaster--bl': position === MessagePosition.BottomLeft,
            'mst-toaster--br': position === MessagePosition.BottomRight,
          })}
        >
          {filtered.map(message => <ToasterItem key={message.id} message={message} />)}
        </div>
      )
      : null;
  }

  render() {
    if (!this.state.messages || this.state.messages.length === 0) {
      return null;
    }

    return (
      <>
        {this.getMessageElements(this.state.messages, MessagePosition.TopLeft)}
        {this.getMessageElements(this.state.messages, MessagePosition.TopRight)}
        {this.getMessageElements(this.state.messages, MessagePosition.BottomLeft)}
        {this.getMessageElements(this.state.messages, MessagePosition.BottomRight)}
      </>
    );
  }
}

export default Toaster;
