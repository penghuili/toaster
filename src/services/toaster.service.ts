import { BehaviorSubject, Observable } from 'rxjs';

import { Message, MessagePosition, MessageType } from '../models/message';

/**
 * `ToasterService` manages message state for `Toaster` component
 */
export const ToasterService = {
  /**
   * A stream of messages, which can emit new messages and also can be subscribed to.
   */
  messages$: new BehaviorSubject<Message[]>([]),

  /**
   * Get the `Observable` of the toaster messages
   */
  getMessages(): Observable<Message[]> {
    return this.messages$.asObservable();
  },

  /**
   * Emit a new toaster message, and remove this new message from state in 5 seconds
   * @param text the content of the new message
   * @param position the position of the new message
   * @param type the type of the new message
   */
  show(text: string, position: MessagePosition, type: MessageType): void {
    const message = {
      id: Date.now(),
      text,
      position,
      type,
    };
    const current = this.messages$.getValue();
    this.messages$.next([...current, message]);

    setTimeout(() => {
      const messages = this.messages$.getValue();
      this.messages$.next(messages.filter(m => m.id !== message.id));
    }, 5000);
  },
};
