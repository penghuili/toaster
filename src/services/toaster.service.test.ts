import { MessagePosition, MessageType } from '../models/message';
import { ToasterService } from './toaster.service';

describe('ToasterService', () => {
  beforeEach(() => {
    ToasterService.messages$.next([]);
  });

  it('should have no message', () => {
    expect.assertions(1);
    ToasterService.getMessages().subscribe((messages) => {
      expect(messages).toEqual([]);
    });
  });
  it('should have 1 message', () => {
    const text = 'test message';
    ToasterService.show(text, MessagePosition.BottomLeft, MessageType.Warning);

    ToasterService.getMessages().subscribe((messages) => {
      expect(messages.length).toBe(1);
      expect(messages[0].text).toBe(text);
      expect(messages[0].position).toBe(MessagePosition.BottomLeft);
      expect(messages[0].type).toBe(MessageType.Warning);
    });
  });
  it('should hide message after 5 sec', () => {
    const text = 'test message 2';
    ToasterService.show(text, MessagePosition.BottomLeft, MessageType.Warning);

    ToasterService.getMessages().subscribe((messages) => {
      expect(messages.length).toBe(1);
      expect(messages[0].text).toBe(text);
      expect(messages[0].position).toBe(MessagePosition.BottomLeft);
      expect(messages[0].type).toBe(MessageType.Warning);
    });

    jest.useFakeTimers();
    jest.advanceTimersByTime(5000);

    ToasterService.getMessages().subscribe((messages) => {
      expect(messages.length).toBe(0);
    });
  });
});
