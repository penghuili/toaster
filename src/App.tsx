import './App.scss';

import React, { Component } from 'react';

import { getClassName } from './functions/class-name';
import { MessagePosition, MessageType } from './models/message';
import { ToasterService } from './services/toaster.service';
import Toaster from './toaster/Toaster';

interface State {
  count: number;
  messageType: MessageType;
}

class App extends Component<{}, State> {
  constructor(props: {}) {
    super(props);
    this.state = {
      count: 0,
      messageType: MessageType.Alert,
    };
  }

  emitTopLeft = () => {
    const count = this.state.count + 1;
    ToasterService.show(
      `top left message: ${count}`,
      MessagePosition.TopLeft,
      this.state.messageType,
    );
    this.setState({ count });
  }
  emitTopRight = () => {
    const count = this.state.count + 1;
    ToasterService.show(
      `top right message: ${count}`,
      MessagePosition.TopRight,
      this.state.messageType,
    );
    this.setState({ count });
  }
  emitBottomLeft = () => {
    const count = this.state.count + 1;
    ToasterService.show(
      `bottom left message: ${count}`,
      MessagePosition.BottomLeft,
      this.state.messageType,
    );
    this.setState({ count });
  }
  emitBottomRight = () => {
    const count = this.state.count + 1;
    ToasterService.show(
      `bottom right message: ${count}`,
      MessagePosition.BottomRight,
      this.state.messageType,
    );
    this.setState({ count });
  }
  changeMessageType = (messageType: MessageType) => {
    this.setState({ messageType });
  }

  render() {
    return (
      <div>
        <div className="mst-button-wrapper">
          <button className="mst-button" onClick={this.emitTopLeft}>top left</button>
          <button className="mst-button" onClick={this.emitTopRight}>top right</button>
        </div>
        <div className="mst-type-hint">
          change toaster message type:
        </div>
        <div className="mst-button-wrapper mst-button-wrapper--type">
          <button
            className={getClassName({
              'mst-button mst-button--round mst-button--alert': true,
              'mst-button--active': this.state.messageType === MessageType.Alert,
            })}
            onClick={() => this.changeMessageType(MessageType.Alert)}
          >
            alert
          </button>
          <button
            className={getClassName({
              'mst-button mst-button--round mst-button--info': true,
              'mst-button--active': this.state.messageType === MessageType.Info,
            })}
            onClick={() => this.changeMessageType(MessageType.Info)}
          >
            info
          </button>
          <button
            className={getClassName({
              'mst-button mst-button--round mst-button--warning': true,
              'mst-button--active': this.state.messageType === MessageType.Warning,
            })}
            onClick={() => this.changeMessageType(MessageType.Warning)}
          >
            warning
          </button>
        </div>
        <div className="mst-button-wrapper">
          <button className="mst-button" onClick={this.emitBottomLeft}>bottom left</button>
          <button className="mst-button" onClick={this.emitBottomRight}>bottom right</button>
        </div>

        <Toaster />
      </div>
    );
  }
}

export default App;
